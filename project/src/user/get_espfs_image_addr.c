/*
 * get_espfs_image_addr.c
 *
 *  Created on: May 3, 2017
 *      Author: sharikov
 */


#include "FreeRTOS.h"
#include "task.h"
#include "diag.h"
#include "objects.h"
#include "flash_api.h"
#include "osdep_service.h"
#include "device_lock.h"
#include "semphr.h"

#include "get_espfs_image_addr.h"




//--- rtl_boot_s.c copy ---
typedef struct _seg_header {
	uint32 size;
	uint32 ldaddr;
} IMGSEGHEAD, *PIMGSEGHEAD;
typedef struct _img2_header {
	IMGSEGHEAD seg;
	uint32 sign[2];
	void (*startfunc)(void);
	uint8 rtkwin[7];
	uint8 ver[13];
	uint8 name[32];
} IMG2HEAD, *PIMG2HEAD;

enum {
	SEG_ID2_ERR,
	SEG_ID2_SRAM,
	SEG_ID2_TCM,
	SEG_ID2_SDRAM,
	SEG_ID2_SOC,
	SEG_ID2_FLASH,
	SEG_ID2_CPU,
	SEG_ID2_ROM,
	SEG_ID2_MAX
} SEG_ID2;

static const char * const txt_tab_seg[] = {
		"UNK",		// 0
		"SRAM",		// 1
		"TCM",		// 2
		"SDRAM",	// 3
		"SOC",		// 4
		"FLASH",	// 5
		"CPU",		// 6
		"ROM"		// 7
};

static const uint32 tab_seg_def[] = { 0x10000000, 0x10070000, 0x1fff0000,
		0x20000000, 0x30000000, 0x30200000, 0x40000000, 0x40800000, 0x98000000,
		0xA0000000, 0xE0000000, 0xE0010000, 0x00000000, 0x00050000 };

static uint32_t get_seg_id(uint32 addr, int32 size) {
	uint32 ret = SEG_ID2_ERR;
	uint32 * ptr = &tab_seg_def;
	if (size > 0) {
		do {
			ret++;
			if (addr >= ptr[0] && addr + size <= ptr[1]) {
				return ret;
			};
			ptr += 2;
		} while (ret < SEG_ID2_MAX);
	};
	return SEG_ID2_ERR;
}

static uint32 load_img2_head(uint32 faddr, PIMG2HEAD hdr) {
	//flashcpy(faddr, hdr, sizeof(IMG2HEAD));
	device_mutex_lock(RT_DEV_LOCK_FLASH);
	flash_stream_read(&flashobj, faddr, sizeof(IMG2HEAD), (uint8_t *)hdr);
	device_mutex_unlock(RT_DEV_LOCK_FLASH);

	uint32 ret = get_seg_id(hdr->seg.ldaddr, hdr->seg.size);
	if (hdr->sign[1] == IMG_SIGN2_RUN) {
		if (hdr->sign[0] == IMG_SIGN1_RUN) {
			ret |= 1 << 9;
		} else if (hdr->sign[0] == IMG_SIGN1_SWP) {
			ret |= 1 << 8;
		};
	}
	if (*(u32 *) (&hdr->rtkwin) == IMG2_SIGN_DW1_TXT) {
		ret |= 1 << 10;
	};
	return ret;
}

static int32_t skip_seqs(int32_t faddr, PIMG2HEAD hdr) {
	int32_t fnextaddr = faddr;
	uint8_t segnum = 0;
	while (1) {
		uint32_t seg_id = get_seg_id(hdr->seg.ldaddr, hdr->seg.size);
		//rtl_printf("fnextaddr=0x%x  seg_id=0x%x\n", fnextaddr, seg_id);
		if (seg_id) {
#if CONFIG_DEBUG_LOG > 2
			rtl_printf("Skip Flash seg%d: 0x%08x -> %s: 0x%08x, size: %d\n", segnum,
					faddr, txt_tab_seg[seg_id], hdr->seg.ldaddr, hdr->seg.size);
#endif
			fnextaddr += hdr->seg.size;
		} else {
			break;
		}
		//fnextaddr += flashcpy(fnextaddr, &hdr->seg, sizeof(IMGSEGHEAD));
		device_mutex_lock(RT_DEV_LOCK_FLASH);
		flash_stream_read(&flashobj, fnextaddr, sizeof(IMGSEGHEAD), (uint8_t *)&hdr->seg);
		device_mutex_unlock(RT_DEV_LOCK_FLASH);
		fnextaddr += sizeof(IMGSEGHEAD);
		segnum++;
	}
	return fnextaddr;
}

static int32_t find_seq_type(int32_t faddr, PIMG2HEAD hdr, int type) {
	int32_t fnextaddr = faddr;
	uint8_t segnum = 0;
	while (1) {
		uint32_t seg_id = get_seg_id(hdr->seg.ldaddr, hdr->seg.size);
		//rtl_printf("fnextaddr=0x%x  seg_id=0x%x\n", fnextaddr, seg_id);
		if ((seg_id == type) || (seg_id == SEG_ID2_ERR))
			break;
		else {
#if CONFIG_DEBUG_LOG > 2
			rtl_printf("Skip Flash seg%d: 0x%08x -> %s: 0x%08x, size: %d\n", segnum,
					fnextaddr, txt_tab_seg[seg_id], hdr->seg.ldaddr, hdr->seg.size);
#endif
			fnextaddr += hdr->seg.size;
		}
		//fnextaddr += flashcpy(fnextaddr, &hdr->seg, sizeof(IMGSEGHEAD));
		device_mutex_lock(RT_DEV_LOCK_FLASH);
		flash_stream_read(&flashobj, fnextaddr, sizeof(IMGSEGHEAD), (uint8_t *)&hdr->seg);
		device_mutex_unlock(RT_DEV_LOCK_FLASH);
		fnextaddr += sizeof(IMGSEGHEAD);
		segnum++;
	}
	return fnextaddr;
}
//---



/* возвращает адрес espfs
 * это следующий сектор после конца прошивки
 * 0xb000 если нулевой прошивки нет
 */
uint32_t get_espfs_image_addr(uint8_t active_img) {
	IMG2HEAD hdr;
	int32_t faddr=0xb000;
	uint32_t img_id;
	uint8_t imgnum=0;
	while (imgnum <= active_img) {
		faddr = (faddr + FLASH_SECTOR_SIZE - 1) & (~(FLASH_SECTOR_SIZE - 1));
		// получим тип первого сегмента прошивки
		img_id = load_img2_head(faddr, &hdr);
		if (imgnum == active_img) {
			faddr = find_seq_type(faddr + 0x10, &hdr.seg, SEG_ID2_ROM);
			break;
		}
		else {
			// заголовок валиден ?
			if ((img_id >> 8) > 4 || (uint8_t) img_id != 0) {
				imgnum++;
				// парсим заголовки всех сегментов этой прошивки
				faddr = skip_seqs(faddr + 0x10, &hdr.seg);
			}
			else {
				if (imgnum == 0) {
					rtl_printf("No Image!\n");
					return 0xb000;
				}
				break;
			}
		}
	}
	//faddr =  (faddr + FLASH_SECTOR_SIZE - 1) & (~(FLASH_SECTOR_SIZE - 1));
	rtl_printf("ESPFS at 0x%x\n", faddr);

	return faddr;
}




