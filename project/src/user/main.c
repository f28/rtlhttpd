#include "FreeRTOS.h"
#include "task.h"
#include "diag.h"
#include "objects.h"
#include "osdep_service.h"
#include "device_lock.h"
#include "semphr.h"
#include "tcm_heap.h"
#include <platform/platform_stdlib.h>
#include "hal_crypto.h"

#include "hal_platform.h"
#include "hal_peri_on.h"
#include "rtl8195a_gpio.h"
#include "wifi_conf.h"
#include "wlan_intf.h"
#include "wifi_constants.h"
#include "wifi_lib.h"

#include <wlan/wlan_test_inc.h>
#include <wifi/wifi_conf.h>
#include <wifi/wifi_util.h>
#include "wlan_lib.h"
#include "wifi_api.h"
#include "main.h"
#include "wc_mgr.h"

#include <lwip_netconf.h>
#include "tcpip.h"
#include <dhcp/dhcps.h>
#include "lwip/tcp_impl.h"

#include "http_server.h"

/* ---------------------------------------------------
 *  Customized Signature (Image Name)
 * ---------------------------------------------------*/
#include "section_config.h"
SECTION(".custom.validate.rodata")
const unsigned char cus_sig[32] = "rtlhttpd";

#ifndef CONFIG_INIT_NET
#define CONFIG_INIT_NET             1
#endif
#ifndef CONFIG_INTERACTIVE_MODE
#define CONFIG_INTERACTIVE_MODE     1
#endif


#define STACKSIZE (512 + 768)



uint32_t check_wifi_settings_reset(void)
{
	gpio_t button_gpio;
	uint32 a,b;
	const uint32_t time=1000;

#if WIFI_RESET_SETTINGS_PIN == PB_1
	HalPinCtrlRtl8195A(LOG_UART, 0, 0);  // disconnect LOG UART
#endif
	gpio_init(&button_gpio, WIFI_RESET_SETTINGS_PIN);
	gpio_dir(&button_gpio, PIN_INPUT);     // Direction: Input
	gpio_mode(&button_gpio, PullUp);       // Pull-High

	a=7;
	for (b=0; b<time; b++) {
		HalDelayUs(1000);
		if (gpio_read(&button_gpio))
			a>>=1;
		else
			a++;
		if (a ==0)
			break;
	}
	gpio_deinit(&button_gpio);
#if WIFI_RESET_SETTINGS_PIN == PB_1
	HalPinCtrlRtl8195A(LOG_UART, 0, 1);  // connect LOG UART
#endif
	HalDelayUs(10000); // uart recovery time
	if (a >= (time - (time>>5)))
		a=1;
	else
		a=0;
	return a;
}

void main(void)
{
	uint32_t wifi_reset;
	//#if DEBUG_MAIN_LEVEL > 0
	//	vPortFree(pvPortMalloc(4)); // Init RAM heap
	//	fATST(NULL); // RAM/TCM/Heaps info
	//#endif
#if DEBUG_MAIN_LEVEL > 2
	ConfigDebugErr  = -1;
	ConfigDebugInfo = -1;
	ConfigDebugWarn = -1;
#endif
	//DBG_ERR_MSG_ON(_DBG_TCM_HEAP_) ;
	//DBG_WARN_MSG_ON(_DBG_TCM_HEAP_) ;
	//DBG_INFO_MSG_ON (_DBG_TCM_HEAP_);



#if defined(CONFIG_CPU_CLK)
	HalCpuClkConfig(0); // 0 - 166666666 Hz, 1 - 83333333 Hz, 2 - 41666666 Hz, 3 - 20833333 Hz, 4 - 10416666 Hz, 5 - 4000000 Hz
	HAL_LOG_UART_ADAPTER pUartAdapter;
	pUartAdapter.BaudRate = RUART_BAUD_RATE_38400;
	HalLogUartSetBaudRate(&pUartAdapter);
	SystemCoreClockUpdate();
	En32KCalibration();
#endif
#if DEBUG_MAIN_LEVEL > 1
	DBG_INFO_MSG_ON(_DBG_TCM_HEAP_); // On Debug TCM MEM
#endif

	if ( rtl_cryptoEngine_init() != 0 )
	{
		DiagPrintf("crypto engine init failed\r\n");
	}

	if (check_wifi_settings_reset())
		wifi_cfg.load_flg=0;

	/* wlan & user_start intialization */
	xTaskCreate(user_init_thrd, "user_init", 1024, NULL, tskIDLE_PRIORITY + 0 + PRIORITIE_OFFSET, NULL);


	DBG_8195A("Main start\n");
	DiagPrintf("\nCLK CPU\t\t%d Hz\nRAM heap\t%d bytes\tTCM heap\t%d bytes\n",
			HalGetCpuClk(), xPortGetFreeHeapSize(), tcm_heap_freeSpace());


	//Enable Schedule, Start Kernel
#if defined(CONFIG_KERNEL) && !TASK_SCHEDULER_DISABLED
#ifdef PLATFORM_FREERTOS
	vTaskStartScheduler();
#endif
#else
	RtlConsolTaskRom(NULL);
#endif

	//    while(1);

}


/* RAM/TCM/Heaps info */
//extern void ShowMemInfo(void);

void ShowMemInfo(void)
{
	rtl_printf("\nCLK CPU\t\t%d Hz\nRAM heap\t%d bytes\nTCM heap\t%d bytes\n",
			HalGetCpuClk(), xPortGetFreeHeapSize(), tcm_heap_freeSpace());
}

// Mem info
void fATST(int argc, char *argv[]) {
	ShowMemInfo();
#if 0 //CONFIG_DEBUG_LOG > 1
	dump_mem_block_list();
	tcm_heap_dump();
#endif
	rtl_printf("\n");
#if (configGENERATE_RUN_TIME_STATS == 1)
	char *cBuffer = pvPortMalloc(512);
	if(cBuffer != NULL) {
		vTaskGetRunTimeStats((char *)cBuffer);
		rtl_printf("%s", cBuffer);
	}
	vPortFree(cBuffer);
#endif
#if defined(configUSE_TRACE_FACILITY) && (configUSE_TRACE_FACILITY == 1) && (configUSE_STATS_FORMATTING_FUNCTIONS == 1)
	{
		char * pcWriteBuffer = malloc(1024);
		if(pcWriteBuffer) {
			vTaskList((char*)pcWriteBuffer);
			rtl_printf("\nTask List:\n");
			rtl_printf(&str_rom_57ch3Dch0A[7]); // "==========================================\n"
			rtl_printf("Name\t  Status Priority HighWaterMark TaskNumber\n%s\n", pcWriteBuffer);
			free(pcWriteBuffer);
		}
	}
#endif
}
void print_udp_pcb(void)
{
	struct udp_pcb *pcb;
	bool prt_none = true;
	rtl_printf("UDP pcbs:\n");
	for(pcb = udp_pcbs; pcb != NULL; pcb = pcb->next) {
		rtl_printf("flg:%02x\t" IPSTR ":%d\t" IPSTR ":%d\trecv:%p\n", pcb->flags, IP2STR(&pcb->local_ip), pcb->local_port, IP2STR(&pcb->remote_ip), pcb->remote_port, pcb->recv );
		prt_none = false;
	}
	if(prt_none) rtl_printf("none\n");
}
/******************************************************************************
 * FunctionName : debug
 * Parameters   :
 * Returns      :
 *******************************************************************************/
extern const char * const tcp_state_str[];
void print_tcp_pcb(void)
{
	struct tcp_pcb *pcb;
	rtl_printf("Active PCB states:\n");
	bool prt_none = true;

	vTaskSuspendAll();
	for(pcb = tcp_active_pcbs; pcb != NULL; pcb = pcb->next) {
		rtl_printf("Port %d|%d\tflg:%02x\ttmr:%p\t%s\n", pcb->local_port, pcb->remote_port, pcb->flags, pcb->tmr, tcp_state_str[pcb->state]);
		prt_none = false;
	}
	xTaskResumeAll();
	if(prt_none) rtl_printf("none\n");
	rtl_printf("Listen PCB states:\n");
	prt_none = true;

	vTaskSuspendAll();
	for(pcb = (struct tcp_pcb *)tcp_listen_pcbs.pcbs; pcb != NULL; pcb = pcb->next) {
		rtl_printf("Port %d|%d\tflg:%02x\ttmr:%p\t%s\n", pcb->local_port, pcb->remote_port, pcb->flags, pcb->tmr, tcp_state_str[pcb->state]);
		prt_none = false;
	}
	xTaskResumeAll();
	if(prt_none) rtl_printf("none\n");
	rtl_printf("TIME-WAIT PCB states:\n");
	prt_none = true;
	vTaskSuspendAll();
	for(pcb = tcp_tw_pcbs; pcb != NULL; pcb = pcb->next) {
		rtl_printf("Port %d|%d\tflg:%02x\ttmr:%p\t%s\n", pcb->local_port, pcb->remote_port, pcb->flags, pcb->tmr, tcp_state_str[pcb->state]);
		prt_none = false;
	}
	xTaskResumeAll();
	if(prt_none) rtl_printf("none\n");
}
void fATLW(int argc, char *argv[]) 	// Info Lwip
{
	print_udp_pcb();
	print_tcp_pcb();
}
MON_RAM_TAB_SECTION COMMAND_TABLE console_commands_main[] = {
		{"ATST", 0, fATST, ": Memory info"},
		{"ATLW", 0, fATLW, ": LwIP Info"}
};
