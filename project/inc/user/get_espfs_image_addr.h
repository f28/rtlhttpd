/*
 * get_espfs_image_addr.h
 *
 *  Created on: May 3, 2017
 *      Author: user003
 */

#ifndef GET_ESPFS_IMAGE_ADDR_H_
#define GET_ESPFS_IMAGE_ADDR_H_

uint32_t get_espfs_image_addr(uint8_t active_img);


#endif /* GET_ESPFS_IMAGE_ADDR_H_ */
