/*
 * http_server.h
 *
 *  Created on: Feb 20, 2017
 *      Author: sharikov
 */

#ifndef HTTP_SERVER_H_
#define HTTP_SERVER_H_


//#define FLASH_APP_BASE  0xd0000

#ifndef FLASH_APP_BASE
 #ifndef RTLHTTPD_IMAGE_NUM
  #define RTLHTTPD_IMAGE_NUM 0
  // RTLHTTPD image number:
  // 0 - default Image; 1..3 - upgraded ImageN
 #endif
#endif


void user_init_thrd(void);

#endif /* HTTP_SERVER_H_ */
