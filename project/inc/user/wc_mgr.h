/*
 * wc_mgr.h
 *
 *  Created on: Feb 16, 2017
 *      Author: sharikov
 */

#ifndef WC_MGR_H_
#define WC_MGR_H_

//#include "mDNS/mDNS.h"
//#include "mdns/example_mdns.h"

#ifndef WLAN0_NAME
#define WLAN0_NAME		"wlan0"
#endif

#ifndef WLAN1_NAME
#define WLAN1_NAME      "wlan1"
#endif


#define WIFI_RESET_SETTINGS_PIN PB_1
// wifi settings reset: connect PB1 to ground

#define AP_SCAN_LIST_SIZE 32




void reset_wifi_settings(void);
void load_wifi_settings(void);
void wc_start(void);

#endif /* WC_MGR_H_ */
