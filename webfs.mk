THISDIR:=$(dir $(abspath $(lastword $(MAKEFILE_LIST))))

#include paths.mk


ESPFS_DIR ?= $(THISDIR)$(BIN_DIR)
 
USE_HEATSHRINK ?= yes
GZIP_COMPRESSION ?= no

HTML_PATH ?= html
webpages.espfs: $(HTML_PATH) $(LIBRTLHTTPD_PATH)espfs/mkespfsimage/mkespfsimage
	cd html; find . | $(THISDIR)$(LIBRTLHTTPD_PATH)espfs/mkespfsimage/mkespfsimage > $(ESPFS_DIR)/webpages.espfs; cd ..
	$(PICK) 0 0 $(ESPFS_DIR)/webpages.espfs $(ESPFS_DIR)/webpages.espfs.p body+reset_offset
#create multi-segment Image
	@cat $(ESPFS_DIR)/ram_2.p.bin >$(ESPFS_DIR)/upg_image.bin
	@cat $(ESPFS_DIR)/webpages.espfs.p >>$(ESPFS_DIR)/upg_image.bin
#end of image marker	
	@printf '\377\377\377\377\377\377\377\377' >>$(ESPFS_DIR)/upg_image.bin
	
$(LIBRTLHTTPD_PATH)espfs/mkespfsimage/mkespfsimage: $(LIBRTLHTTPD_PATH)espfs/mkespfsimage/
	$(MAKE) -C $(LIBRTLHTTPD_PATH)espfs/mkespfsimage USE_HEATSHRINK="$(USE_HEATSHRINK)" GZIP_COMPRESSION="$(GZIP_COMPRESSION)" all
	
clean_webfs:
	$(MAKE) -C $(LIBRTLHTTPD_PATH)espfs/mkespfsimage/ -f Makefile clean
	rm -rf $(ESPFS_DIR)/webpages.espfs.*	
	rm -rf $(ESPFS_DIR)/upg_image.bin